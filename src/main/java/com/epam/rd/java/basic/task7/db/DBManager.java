package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static Connection connection;
	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance==null){
			instance = new DBManager();
			FileInputStream fis;
			Properties property = new Properties();
			try {
				fis = new FileInputStream("app.properties");
				property.load(fis);
				String connectionUrl = property.getProperty("connection.url");
				String URL = "jdbc:mysql://localhost:3306/mydbtest";
				String PASS= "0000";
				String USER = "root";
				//connection = DriverManager.getConnection(URL,USER,PASS);
				connection = DriverManager.getConnection(connectionUrl);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> list = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM users");
			while(resultSet.next()){
				User user = new User();
				user.setLogin(resultSet.getString("login"));
				user.setId(resultSet.getInt("id"));
				list.add(user);
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}
		return list;
	}

	public boolean insertUser(User user) throws DBException {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("INSERT INTO users (login) VALUES(?)");

			preparedStatement.setString(1,user.getLogin());

			boolean flag = preparedStatement.execute();
			preparedStatement = connection.prepareStatement("SELECT id FROM users where login =?");
			preparedStatement.setString(1,user.getLogin());
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			user.setId(resultSet.getInt("id"));

			return flag;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}

	}

	public boolean deleteUsers(User... users) throws DBException {
		for (User user:users) {
			PreparedStatement preparedStatement = null;
			PreparedStatement preparedStatement1 = null;
			try {
				preparedStatement = connection.prepareStatement("DELETE FROM users where id=?");
			preparedStatement.setInt(1,user.getId());
			preparedStatement.executeUpdate();

			preparedStatement1 = connection.prepareStatement("DELETE FROM users_teams where user_id=?");
			preparedStatement1.setInt(1,user.getId());
			preparedStatement1.executeUpdate();
			} catch (SQLException e) {
				throw new DBException(e.getMessage(),e.getCause());
			}
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users where login = ?");
			preparedStatement.setString(1, login);
			ResultSet resultSet = preparedStatement.executeQuery();

				User user = new User();
				resultSet.next();
				user.setLogin(resultSet.getString("login"));
				user.setId(resultSet.getInt("id"));
				return user;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}

	}

	public Team getTeam(String name) throws DBException {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM teams where name = ?");
			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();

			Team team = new Team();
			resultSet.next();
			team.setName(resultSet.getString("name"));
			team.setId(resultSet.getInt("id"));
			return team;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}

	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> list = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM teams");
			while(resultSet.next()){
				Team team = new Team();
				team.setName(resultSet.getString("name"));
				list.add(team);
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}
		return list;
	}

	public boolean insertTeam(Team team) throws DBException {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("INSERT INTO teams (name) VALUES(?)");

			preparedStatement.setString(1,team.getName());
			boolean flag = preparedStatement.execute();
			preparedStatement = connection.prepareStatement("SELECT id FROM teams where name =?");
			preparedStatement.setString(1,team.getName());
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			team.setId(resultSet.getInt("id"));
			return flag;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}

	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Savepoint savepoint = null;
		try {
			connection.setAutoCommit(false);
			savepoint = connection.setSavepoint("savepoint");
			for (Team team : teams) {
				PreparedStatement preparedStatement = null;

				preparedStatement = connection.prepareStatement("INSERT INTO users_teams(user_id,team_id) VALUES(?,?)");

				preparedStatement.setInt(1, user.getId());
				preparedStatement.setInt(2, team.getId());
				preparedStatement.executeUpdate();
			}
			connection.commit();
			connection.setAutoCommit(true);
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback(savepoint);
				connection.setAutoCommit(true);

			} catch (SQLException ex) {
				throw new DBException(ex.getMessage(),ex.getCause());
			}
			throw new DBException(e.getMessage(),e.getCause());
		}

	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> list = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT team_id FROM users_teams where user_id=?");
			preparedStatement.setInt(1,user.getId());
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT * FROM teams where id = ?");
				preparedStatement1.setInt(1,resultSet.getInt("team_id"));
				ResultSet resultSet1 = preparedStatement1.executeQuery();
				if(resultSet1.next()) {
					Team team = new Team();
					team.setId(resultSet1.getInt("id"));
					team.setName(resultSet1.getString("name"));
					list.add(team);
				}
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}

		return list;
	}

	public boolean deleteTeam(Team team) throws DBException {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("DELETE FROM teams where id =?");


		preparedStatement.setInt(1,team.getId());
		preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("UPDATE teams SET name = ? WHERE id=?");

		preparedStatement.setString(1,team.getName());
		preparedStatement.setInt(2,team.getId());
		preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}
		return false;
	}

}
